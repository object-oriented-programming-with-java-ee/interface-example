package demo;

public class Main {

	public static void main(String[] args) {
		
		Kitchen myKitchen = new KitchenImpl();
		
		Vegetable carrot = new CarrotImpl();
		
		Vegetable broccoli = new BroccoliImpl();
				
		Vegetable[] veggies = {carrot, broccoli};
		
		myKitchen.switchOnLight();
		
		myKitchen.chopVegetables(veggies);
	}

}
