package demo;

public interface Kitchen extends Room {

	public void washDishes();
	
	public void chopVegetables(Vegetable[] veggies);
	
	public void openWindow();
	
	public void turnStoveOn();
	
}
